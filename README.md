Mastodon Plugin
================================
Mastodon Plugin for the [**Bludit CMS**](https://www.bludit.com/)

Publish new Bludit posts on Mastodon or Friendica (via Mastodon API).

***Before you start***:
Please read [here](docs/publish.md) about publishing methods of this plugin.

Requires PHP Curl Extension

Using [TootoPHP](https://framagit.org/MaxKoder/TootoPHP)

License
-------
This is open source software licensed under the [MIT license](https://tldrlegal.com/license/mit-license).
