Publishing Methods
================================

There are two ways to publish your posts:

Recommended: Cronjob
-------
If you have the possibility: Setup a cronjob which calls your url (e.g. with curl) with the path `/runMastodonQueue` (every minute). This will publish all posts which are due to be published.

Example:
```
* * * * * curl https://yourdomain.com/runMastodonQueue
```

There are more ways to setup a cronjob, but this is the most common one. E.g. you can setup it in tools like Plesk or cPanel or use a service provider for cronjobs.

Alternative: Enable auto-post in Plugin settings
-------
You can enable auto-post in the plugin settings. This will publish all posts which are due to be published when a user visits your website (and on some backend actions).

***Warning***:
This is not recommended, because it can slow down your website and it is not guaranteed that all posts will be published in a timely manner.
For frequently visited websites this method can lead to duplicates.
But it is a good way to test the plugin.

