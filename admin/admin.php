<h2 class="mt-0 mb-3">Mastodon</h2>

<table class="table table-striped mt-3">
    <thead>
    <tr>
        <th><?php echo $L->get('ms_page'); ?></th>
        <th><?php echo $L->get('date'); ?></th>
        <th><?php echo $L->get('ms_status'); ?></th>
        <th><?php echo $L->get('ms_response'); ?></th>
        <th></th>
    </tr>
    </thead>

    <?php
    foreach ($this->getQueueSortedByDate() as $key => $entry) {
        $response = '';
        if(isset($entry['result'])){
            $response ='<details>
    <summary>
       '.$L->get('ms_response').'
    </summary>
    <p>
        '.print_r($entry['result'],true).'
    </p>
</details>';
        }
        echo '<tr><td>' . $key . '</td><td>' . date('Y-m-d H:i:s', $entry['date']) . '</td><td>' . $this->displaybleStatus($this->getItemStatus($entry)) . '</td><td>'.$response.'</td><td>'.$this->getButtons($key,$entry).'</td></tr>';
    }
    ?>
</table>