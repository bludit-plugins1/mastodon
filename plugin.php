<?php

class pluginMastodon extends Plugin {

    public const STATUS_WAITING = 'ms_status_waiting';
    public const STATUS_OK = 'ms_status_ok';
    public const STATUS_UNKOWN = 'ms_status_unkown';
    public const STATUS_ERROR = 'ms_status_error';
    public const STATUS_IGNORE = 'ms_status_ignore';

    public function init()
    {
        // Fields and default values for the database of this plugin
        $this->dbFields = array(
            'domain' => 'mastodon.social',
            'app_name' => '',
            'app_url' => '',
            'api_credentials' => '',
            'mastodon_token' => '',
            'test_message' => false,
            'template' => "{title} - {description} \n{tags}\n\n{url}",
            'queue' => '',
            'enable_autopost' => false,
            'char_limit' => 500
        );

        require_once 'autoload.php';
    }


	// Method called on the settings of the plugin on the admin area
	public function form()
    {
		global $L;
        global $site;
        $disableAuth = false;
        if($this->getValue('app_name') == ''){
            $disableAuth = true;
            $appName = (string)$site->title().' Mastodon Plugin';
        }else{
            $appName = $this->getValue('app_name');
        }
        if($this->getValue('app_url') == ''){
            $appUrl = (string)$site->url();
        }else{
            $appUrl = $this->getValue('app_url');
        }
        $html = '<div>';
        $html .= '<input name="app_name" type="hidden" value="'.$appName.'">';
        $html .= '<input name="app_url"  type="hidden" value="'.$appUrl.'">';
        $html .= '</div>';

		$html .= '<div>';
		$html .= '<label>Mastodon Domain</label>';
		$html .= '<input name="domain" type="text" value="'.$this->getValue('domain').'">';
        $html .= '<span class="tip">'.$L->get('attention_please_save').'</b></span>';
		$html .= '</div>';


        if(!$disableAuth) {
            $html .= '<br><br><div>';
            $html .= '<b>App Name:</b> '.$this->getValue('app_name').'<br>';
            $html .= '<b>App URL:</b> '.$this->getValue('app_url').'<br>';
            $html .= '<a href="#" data-toggle="modal" data-target="#mastoauthmodal"  class="btn btn-primary btn-md" ' . (($disableAuth) ? 'disabled' : '') . '>'.$L->get('authorize_at') ." ". $this->getValue('domain') . '</a>';
            $html .= '</div>';
            $tootoPHP = new TootoPHP($this->getValue('domain'));
            $tootoPHP->setBluditPlugin($this);
            $app = $tootoPHP->registerApp($this->getValue('app_name'), $this->getValue('app_url'));

            $link = $app->getAuthUrl();
            $html .= '<div id="mastoauthmodal" class="modal fade" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                <p>'.$L->get('please_open_link').'</p>
                                <p><a href="'.$link.'" target="_blank">'.$link.'</a></p>
                                <p>'.$L->get('auth_this_app').':</p>

                                    <input type="text" name="mastodon_token"><br><br>
                                    <input type="submit" value="'.$L->get('save').'">

                            </div>
                        </div>
                    </div>
                </div><br>';
            if(strlen($this->getValue('mastodon_token')) > 3) {
                $result = $app->registerAccessToken($this->getValue('mastodon_token'));
                if ($result !== false) {
                    $html .= '<div class="alert-success">&nbsp;';
                    $html .= $L->get('authorization_successful').'!';
                    $html .= '</div><br><br>';
                } else {
                    $html .= '<div class="alert-danger">&nbsp;';
                    $html .= $L->get('authorization_error').'!';
                    $html .= '</div><br><br>';
                }
                $this->setField('mastodon_token','');
            }

            $html .= '<input type="submit"  class="btn btn-primary btn-md" name="test_message" value="Test post">';
            if($this->getValue('test_message') != false){
                $this->setField('test_message',0);
                $app->postStatus("This is a test status from Mastodon plugin for Bludit\n\nhttps://gitlab.com/bludit-plugins1/mastodon");
            }


            $html .= '<br><br><div>';
            $html .= '<input type="hidden" name="enable_autopost" value="0" />';
            $html .= '<input name="enable_autopost" type="checkbox" value="1" '.($this->getValue('enable_autopost') == true ? 'checked' : '').'> '.$L->get('enable_autopost');
            $html .= '<span class="tip">'.$L->get('warning_enable_autopost').'</b></span>';
            $html .= '</div>';

        }else{
            $html .= '<br><br><div class="alert alert-warning">';
            $html .= '<b>'.$L->get('attention').':</b> '.$L->get('have_to_save');
            $html .= '</div>';
        }

        $html .= '<div>';
        $html .= '<label>Template</label>';
        $html .= '<textarea name="template" type="text">'.$this->getValue('template').'</textarea>';
        $html .= '<span class="tip">'.$L->get('variables').': {title},{description},{tags},{url},{content}</b></span>';
        $html .= '</div>';

        $html .= '<div>';
        $html .= '<label>'.$L->get('char_limit').'</label>';
        $html .= '<input name="char_limit" type="number" value="'.$this->getValue('char_limit').'">';
        $html .= '<span class="tip">'.$L->get('zero_disable').'</span>';
        $html .= '</div>';

        $queue = '<h3>Queue</h3><table class="table table-striped mt-3"><thead><tr><th>Page</th><th>Date</th><th>Posted</th></tr></thead>';
        foreach ($this->getQueue() as $key => $entry)
        {
            $queue .= '<tr><td>'.$key.'</td><td>'.date('Y-m-d H:i:s',$entry['date']).'</td><td>'.$entry['posted'].'</td></tr>';
        }
        $queue .= '</table>';

        $html .= '<br><br><details>
                  <summary>
                    Debugging
                  </summary>
                  <p>
                    '.$queue.'
                  </p>
                </details>';

		return $html;
	}

    public function updateQueue($key){
        global $pages;
        $page = $pages->getPageDB($key);
        if($page === false || !in_array($page['type'],['published','scheduled'])){
            $this->removeFromQueue($key);
        }else{
            $this->addToQueue($key);
        }
    }

    protected function removeFromQueue($key){
        $queue = $this->getQueue();
        if(array_key_exists($key,$queue)){
            unset($queue[$key]);
        }
        $this->setQueue($queue);
    }

    public function addToQueue($key){
        global $pages;
        $queue = $this->getQueue();
        $page = $pages->getPageDB($key);
        $offset = "";
        if(!array_key_exists($key,$queue)){
            $queue[$key] = array("date","posted"=>0);
            $offset = " + 1 minute";
        }

        $queue[$key]["date"] = strtotime($page["date"].$offset);
        $this->setQueue($queue);
    }

    public function runQueue()
    {
        global $pages;
        $queue = $this->getQueue();
        foreach ($queue as $key => $item) {
            if($item['date'] <= time() && $item['posted'] == 0 && $item['date'] > strtotime("- 6 hours")){
                $page = $pages->getPageDB($key);
                if($page !== false) {
                    $queue[$key]["posted"] = 1;
                    $this->setQueue($queue);
                    $content = $this->renderPageStatus($key);
                    $result = $this->pageToStatus($key,$content);
                    $queue[$key]["result"] = $result;
                    $this->setQueue($queue);
                }else{
                    $this->removeFromQueue($key);
                }
            }
            if($item['date'] < strtotime("- 1 week") && $item['posted'] == 1){
                $this->removeFromQueue($key);
            }
            if($item['date'] < strtotime("- 1 month")){
                $this->removeFromQueue($key);
            }
        }
    }

    public function adminBodyBegin()
    {
        global $url;
        global $L;
        $URI = $url->uri();
        $limit = $this->getValue('char_limit');
        if($limit > 0 && strpos($URI,'/'.ADMIN_URI_FILTER.'/edit-content') !== false){
            $key = substr($URI,20);
            $length = strlen($this->renderPageStatus($key));
            if($length > $limit) {
                $preview = '<details>
    <summary>
        '.$L->get('Preview').'
    </summary>
    <p>
        '.$this->renderPageStatus($key).'
    </p>
</details>';
                echo '<div class="alert alert-warning"><b>'.$L->get('char_warn_warn').'</b>: '.sprintf($L->get('char_warn_message'),$length,$limit).' '.$preview.'</div>';
            }
        }
    }


    public function afterPageCreate($key ='')
    {
        if($key !== ''){
            $this->updateQueue($key);
        }
        if($this->getValue('enable_autopost') == true){
            $this->runQueue();
        }
    }

    public function afterPageModify($key ='')
    {
        if($key !== ''){
            $this->updateQueue($key);
        }
        if($this->getValue('enable_autopost') == true){
            $this->runQueue();
        }
    }

    public function beforeAll()
    {
        $webhook = 'runMastodonQueue';
        if ($this->webhook($webhook)) {
            $this->runQueue();
            exit(0);
        }
        if ($this->webhook('showRawQueue')) {
            $queue = $this->getQueue();
            echo '<pre>';
            var_dump($queue);
            exit(0);
        }
        if($this->getValue('enable_autopost') == true){
            $this->runQueue();
        }
    }


    public function adminSidebar()
    {
        $errorCount = 0;
        $queue = $this->getQueue();
        foreach ($queue as $entry){
            $status = $this->getItemStatus($entry);
            if($status == self::STATUS_ERROR){
                $errorCount++;
            }
        }
        $bagde = '';
        if($errorCount > 0) {
            $bagde = ' <span class="badge badge-danger badge-pill">'.$errorCount.'</span>';
        }
        $pending = '';
        $html = '<a class="nav-link" href="'.HTML_PATH_ADMIN_ROOT.'plugin/pluginMastodon">Mastodon '.$bagde.'</a>';
        return $html;
    }

    public function adminView(){
        global $L;
        if ($_POST['action']==='ignore') {
            $this->ignore($_POST['key']);
        }
        if ($_POST['action']==='resend') {
            $this->resend($_POST['key']);
        }
        include __DIR__ . DS . 'admin'.DS.'admin.php';
    }


    public function renderPageStatus($key)
    {
        global $site;
        global $pages;
        $page = $pages->getPageDB($key);


        $template = $this->getValue('template');
        $text = str_replace('{title}',$page['title'],$template);
        $text = str_replace('{description}',$page['description'],$text);
        $text = str_replace('{url}',$site->url().'/'.$key,$text);
        $pageObject = new Page($key);
        $text = str_replace('{content}',$pageObject->content(),$text);
        if(count($page['tags']) > 0) {
            foreach($page['tags'] as $tkey => $tag){
                $page['tags'][$tkey] = str_replace(' ','_',$tag);
                $page['tags'][$tkey] = str_replace('-','_',$page['tags'][$tkey]);
            }
            $text = str_replace('{tags}', '#'. implode(' #', $page['tags']), $text);
        }else{
            $text = str_replace('{tags}', '',$text);
        }
        return htmlspecialchars_decode($text);
    }

    protected function pageToStatus($key,$text)
    {
        global $pages;

        $tootoPHP = new TootoPHP($this->getValue('domain'));
        $tootoPHP->setBluditPlugin($this);
        $app = $tootoPHP->registerApp($this->getValue('app_name'), $this->getValue('app_url'));
        $result = $app->postStatus($text);
        if(isset($result['uri'])) {
            $postUrl = $result['uri'];
            $this->createCustomFieldIfNotExists('mastodon_post_url');
            $pages->edit(['key' => $key, 'custom' => ['mastodon_post_url' => $postUrl]]);
        }
        return $result;
    }

    protected function getQueue(){
        $queue = $this->getValue('queue');
        $queue = base64_decode($queue);
        $queue = unserialize($queue);
        if(!is_array($queue)){
            $queue = array();
        }
        return $queue;
    }


    protected function getQueueSortedByDate()
    {
        $queue = $this->getQueue();
        uasort($queue, function ($item1, $item2) {
            return $item2['date'] <=> $item1['date'];
        });

        return $queue;
    }

    protected function setQueue($value){
        $value = serialize($value);
        $value = base64_encode($value);
        $this->setField("queue",$value);
    }
    protected function createCustomFieldIfNotExists($name){
        global $site;
        $customFields = $site->customFields();
        if(!is_array($customFields) || !array_key_exists($name,$customFields)){
            $newField = new \stdClass();
            $newField->type = 'string';
            $newField->label = 'Mastodon Post URL';
            $newField->tip = 'Filled automatically by Mastodon plugin';
            $customFields[$name] = $newField;
            $customFieldsEncoded = json_encode($customFields);
            $site->set(array('customFields'=>$customFieldsEncoded));
        }
    }

    public function getItemStatus($entry)
    {
        if((int)$entry['posted']===0){
            return self::STATUS_WAITING;
        }
        if((int)$entry['posted']===2){
            return self::STATUS_IGNORE;
        }
        if(!isset($entry['result'])){
            return self::STATUS_UNKOWN;
        }
        if(isset($entry['result']['id']) && strlen($entry['result']['id']) > 1){
            return self::STATUS_OK;
        }
        if(isset($entry['result']['error']) && strlen($entry['result']['error']) > 1){
            return self::STATUS_ERROR;
        }
        return self::STATUS_UNKOWN;
    }

    public function displaybleStatus($status)
    {
        global $L;
        $html = '<div class="text-center alert-';
        switch ($status){
            case self::STATUS_WAITING:
                $html .= "secondary";
                break;
            case self::STATUS_ERROR:
                $html .= "danger";
                break;
            case self::STATUS_OK:
                $html .= "primary";
                break;
            default:
            case self::STATUS_UNKOWN:
                $html .= "warning";
                break;
        }
        $html .= '">'.$L->get($status).'</div>';
        return $html;
    }

    public function getButtons($key,$entry)
    {
        global $L;
        $status = $this->getItemStatus($entry);
        if($status == self::STATUS_ERROR){
            $form = '<form method="post" action="/'.ADMIN_URI_FILTER.'/plugin/pluginMastodon" style="display: inline"><input type="hidden" name="key" value="'.$key.'">';
            $form .= '<input type="hidden" name="action" value="ignore">';
            $form .= '<input type="hidden" name="tokenCSRF" value="'.Session::get('tokenCSRF').'">';
            $form .= '<input type="submit" class="btn btn-sm btn-secondary" value="'.$L->get('ms_btn_ignore').'">';
            $form .= '</form> ';
            $form .= '<form method="post"  action="/'.ADMIN_URI_FILTER.'/plugin/pluginMastodon"  style="display: inline"><input type="hidden" name="key" value="'.$key.'">';
            $form .= '<input type="hidden" name="tokenCSRF" value="'.Session::get('tokenCSRF').'">';
            $form .= '<input type="hidden" name="action" value="resend">';
            $form .= '<input type="submit" class="btn btn-sm btn-secondary" value="'.$L->get('ms_btn_resend').'">';
            $form .= '</form>';
            return $form;
        }
    }

    public function ignore($key)
    {
        $queue = $this->getQueue();
        $queue[$key]['posted'] = 2;
        $this->setQueue($queue);
    }
    public function resend($key)
    {
        $queue = $this->getQueue();
        $queue[$key]['posted'] = 0;
        unset($queue[$key]['result']);
        $queue[$key]['date'] = time();
        $this->setQueue($queue);
    }


}
